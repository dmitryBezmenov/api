<?php

namespace Msnet\Api\Engine;

use Msnet\Api\Engine\Options\Base;
use Msnet\Api\Engine\Options\User;

class Options extends Base
{
    /**
     * @var object $user
     */
    public $user;

    /**
     * 
     */
    public function __construct()
    {
        $this->user = new User();
    }
}