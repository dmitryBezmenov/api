<?php

namespace Msnet\Api\Engine\Options;

use Msnet\Api\Engine\Options\Base;

class User extends Base
{
    /**
     * @var bool $authorized
     */
    public $authorized = false;
}