<?php

namespace Msnet\Api\Engine\Options;

class Base
{
    /**
     * @param string $name
     * @param mixed  $value
     */
    public function set(string $name, $value)
    {
        $parts = explode(".", $name);
        $first = array_shift($parts);

        if (!isset($this->{$first})) {
            throw new \Exception("Invalid option: $first");
        }

        $option = &$this->{$first};

        if (!empty($parts)) {
            if (!($option instanceof self))
                throw new \Exception("Invalid type of option: $first");

            return $option->set(join(".", $parts), $value);
        }

        $option = $value;
    }

    /**
     * @param string $name
     * @param mixed  $value
     */
    public function get(string $name)
    {
        $parts = explode(".", $name);
        $first = array_shift($parts);

        if (!isset($this->{$first})) {
            throw new \Exception("Invalid option: $first");
        }

        $option = $this->{$first};

        if (!empty($parts)) {
            if (!($option instanceof self))
                throw new \Exception("Invalid type of option: $first");

            return $option->get(join(".", $parts));
        }

        if (is_callable($option))
            return $option();
        else
            return $option;
    }
}