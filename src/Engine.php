<?php

namespace Msnet\Api;

use Msnet\Api\State;
use Msnet\Api\Response;
use Msnet\Api\Context;
use Msnet\Api\Response\Error;
use Msnet\Api\Engine\Options;
use Symfony\Component\HttpFoundation\Request;

class Engine
{
    public static function run(Request $request, Options $options)
    {
        $data = json_decode(
            $request->getContent()
        );

        if (empty($data) || !is_array($data)) {
            return Response::error(1, "Empty or invalid request content");
        }

        $response = new Context();

        foreach($data as $action) 
        {
            if (
                !is_object($action) 
                || !property_exists($action, "method") || !is_string($action->method) 
                || !property_exists($action, "params") || !is_object($action->params) 
            ) {
                $response->addItem(
                    Response::error(2, "Invalid structure of method request")
                );
                continue;
            }

            if (!($method = State::getMethodByName($action->method))) 
            {
                $response->addItem(
                    Response::error(3, "Method '{$action->method}' not found")
                );
                continue;
            }

            if ($method->needAuth() && !$options->get("user.authorized"))
            {
                $response->addItem(
                    Response::error(7, "Not authorized")
                );
                continue;
            }

            try {
                $params = $response->fill($action->params);
            } 
            catch (\Exception $e) 
            {
                $response->addItem(
                    Response::error(5, "Method contains invalid link, execution cancelled. {$e->getMessage()}")
                );
                continue;
            }
            
            $method->init($params);

            if (!$method->validateParams()) 
            {
                $response->addItem(
                    Response::error(2, "Invalid structure of method parameters")
                );
                continue;
            }

            try
            {
                $result = $method->executeByEngine();
            }
            catch (\Exception $e)
            {
                $response->addItem(
                    Response::error(6, env("APP_DEBUG") ? $e->getMessage() : "Internal error")
                );
                continue;
            }

            if ($result instanceof Error) {
                $response->addItem($result->value());
                continue;
            }

            if (!$method->validateResult()) {
                $response->addItem(
                    Response::error(4, "Internal error: method returns invalid structure of response")
                );
            } else {
                $response->addItem(
                    Response::success($result)
                );
            }
        }

        return $response->getJson();
    }
}