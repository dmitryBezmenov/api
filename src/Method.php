<?php

namespace Msnet\Api;

use Symfony\Component\HttpFoundation\Request;
use Msnet\Api\Method\Structure;

class Method
{
    /**
     * @var object $params
     */
    protected $params;

    /**
     * @var object $result
     */
    protected $result;

    /**
     * @var bool $needAuth
     */
    protected $needAuth = false;

    /**
     * @var object $params
     */
    public function init($params)
    {
        $this->params = $params;
        $this->result = json_decode(json_encode((object)$this->result()));
    }

    /**
     * @return bool
     */
    public function validateParams()
    {
        $required = new Structure((object)$this->params());
        $received = new Structure($this->params);

        return $required->isIdentical($received);
    }

    /**
     * @return bool
     */
    public function validateResult()
    {
        $required = new Structure((object)$this->result());
        $fetched = new Structure($this->result);

        return $required->isIdentical($fetched);
    }

    /**
     * @return object
     */
    public function executeByEngine()
    {
        $error = $this->execute();

        return $error ?: $this->result;
    }

    /**
     * @return array 
     * 
     * Содержит структуру параметров метода
     */
    public function params()
    {
        return (object)[];
    }

    /**
     * @return array
     * 
     * Содержит структуру результата работы метода
     */
    public function result()
    {
        return (object)[];
    }

    /**
     * Исполнение метода (вызывается контроллером)
     */
    public function execute()
    {
        $this->result = $this->params;
    }

    /**
     * @return string
     * 
     * Краткое описание метода
     */
    public function desc()
    {
        return "";
    }

    /**
     * @return bool
     */
    public function needAuth()
    {
        return $this->needAuth;
    }
}
