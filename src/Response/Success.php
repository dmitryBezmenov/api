<?php

namespace Msnet\Api\Response;

class Success
{
    /**
     * @var array|object $data
     */
    protected $data;

    /**
     * @var array|object $data
     */
    public function __construct($data)
    {
        if (!is_array($data) && !is_object($data))
            throw new \Exception("Invalid type of parameter");

        $this->data = $data;
    }

    /**
     * @return object
     */
    public function value()
    {
        return json_decode(json_encode([
            'status' => 1,
            'data' => (object)$this->data   
        ]));
    }
}
