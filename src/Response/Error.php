<?php

namespace Msnet\Api\Response;

class Error
{
    /**
     * @var string $code
     */
    protected $code;

    /**
     * @var string $message
     */
    protected $message;

    /**
     * @var string $code
     * @var string $message
     */
    public function __construct(string $code, string $message)
    {
        $this->code = $code;
        $this->message = $message;
    }

    /**
     * @return object
     */
    public function value()
    {
        return json_decode(json_encode([
            'status' => 0,
            'error' => [
                'code' => (int)$this->code,
                'message' => $this->message
            ]            
        ]));
    }
}
