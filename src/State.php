<?php

namespace Msnet\Api;

use Msnet\Api\Method;

class State
{
    public static function getMethods()
    {
        $directory = config("api.methods.directory");
        $namespace = str_replace("\\", ".", $directory);
        $rootDir = $_SERVER['DOCUMENT_ROOT'] . '/../';
        $matchExpr = $rootDir . trim($directory, "/") . "/*.php";

        $files = rglob($matchExpr);
        $result = [];

        foreach ($files as $file)
        {
            $filename = substr($file, strlen($rootDir));
            $filename = substr($filename, 0, -4);
            $className = "";

            foreach (explode("/", $filename) as $part)
            {
                $className .= ucfirst($part) . "\\";
            }

            $className = substr($className, 0, -1);

            try {
                $method = new $className();
            } catch(\Exception $e) {
                continue;
            }
            
            if (!($method instanceof Method))
                continue;

            $methodName = str_replace("\\", ".",  
                strtolower(
                    substr($className, strlen($namespace))
                )
            );

            $result[$methodName] = $method;
        }

        return $result;
    }

    /**
     * @return Method
     */
    public static function getMethodByName(string $name)
    {
        $class = 
            rtrim(join("\\", array_map("ucfirst", explode("/", config("api.methods.directory")))), "\\")
            . "\\" . join("\\", array_map("ucfirst", explode(".", $name)));

        if (!class_exists($class)) {
            return false;
        }
        
        return new $class();
    }
}

function rglob($pattern, $flags = 0)
{
    $files = glob($pattern, $flags);

    foreach (glob(dirname($pattern).'/*', GLOB_ONLYDIR|GLOB_NOSORT) as $dir) {
        $files = array_merge($files, rglob($dir.'/'.basename($pattern), $flags));
    }

    return $files;
}
