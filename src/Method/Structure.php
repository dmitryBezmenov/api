<?php

namespace Msnet\Api\Method;

class Structure
{
    /**
     * @var mixed $data
     */
    protected $data;

    /**
     * @param mixed $data
     */
    public function __construct($data)
    {
        $this->data = json_decode(json_encode($data));
    }

    /**
     * @return mixed
     */
    public function getData()
    {
        return $this->data;
    }

    /**
     * @var Structure $another
     * 
     * @return bool
     */
    public function isIdentical(Structure $another)
    {
        $corData = $another->getData();

        if (is_object($this->data)) 
        {
            if (!is_object($corData))
                return false;

            foreach ($this->data as $key => $value)
            {
                if (!isset($corData->{$key}))
                    return false;
                    
                $part = new Structure($value);
                $corPart = new Structure($corData->{$key});

                if (!$part->isIdentical($corPart))
                    return false;
            }

            return true;
        }

        if (is_array($this->data))
        {
            if (!is_array($corData))
                return false;

            if (empty($this->data) || empty($corData))
                return true;

            $part = new Structure($this->data[0]);
            $corPart = new Structure($corData[0]);

            if (!$part->isIdentical($corPart))
                return false;

            return true;
        }

        if (gettype($this->data) !== gettype($corData))
            return false;
        
        return true;
    }
}