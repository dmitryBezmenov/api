<?php

namespace Msnet\Api;

class Context
{
    /**
     * @var array $data
     */
    protected $data;

    public function __construct()
    {
        $this->data = [];
    }

    /**
     * @param object $item
     */
    public function addItem(object $item)
    {
        $this->data[] = $item;
    }

    /**
     * Fill request parameters from context contains results of previous calls
     * 
     * @param $params
     */
    public function fill($params)
    {
        if (is_object($params)) 
        {
            foreach ($params as $key => &$value)
            {
                $value = $this->fill($value);
            }
        }

        if (is_array($params))
        {
            foreach ($params as &$item)
            {
                $item = $this->fill($item);
            }
        }

        if (is_string($params) && strlen($params) && $params[0] == '&') 
        {
            $parts = explode(".", substr($params, 1));
            $item = array_shift($parts);

            if (!strlen($item)) {
                throw new \Exception("Empty index of data item");
            }

            $data = $this->getData();

            if (!isset($data[$item])) {
                throw new \Exception("Data item at index {$item} not found, current length of data: " . count($data));
            }

            $data = $data[$item]->data;

            foreach ($parts as $part) 
            {
                if (preg_match("/\[(\d+)]/", $part, $match)) 
                {
                    $index = $match[1];

                    if (!isset($data[$index])) {
                        throw new \Exception("No data at index {$index}");
                    }

                    $data = $data[$index];
                } 
                else 
                {
                    if (!property_exists($data, $part)) {
                        throw new \Exception("Key {$part} not found");
                    }

                    $data = $data->{$part};
                }
            }

            $params = $data;
        }

        return $params;
    }

    /**
     * @return array
     */
    public function getData()
    {
        return $this->data;
    }

    /**
     * @return string
     */
    public function getJson()
    {
        return json_encode($this->getData());
    }
}