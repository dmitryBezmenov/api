<?php

namespace Msnet\Api;

use Msnet\Api\Response as Responses;

class Response
{
    /**
     * @return object
     */
    public static function error(int $code, string $message)
    {
        return (new Responses\Error($code, $message))->value();
    }

    /**
     * @param array|object $data
     * 
     * @return object
     */
    public static function success($data)
    {
        return (new Responses\Success($data))->value();
    }
}