<?php

namespace App\Api/*NAMESPACE*/;

use Msnet\Api\Method;
use Msnet\Api\Response\Error;
use Auth;
use Illuminate\Validation\Validator;

class /*CLASS*/ extends Method
{
    protected $needAuth = false;

    public function desc()
    {
        return '';
    }

    public function params()
    {
        return [
            
        ];
    }

    public function result()
    {
        return [

        ];
    }

    public function execute()
    {
        $this->result = $this->params;
    }
}